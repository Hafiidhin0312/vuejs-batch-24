var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

time=10000 //10 detik
function execute(time, books, i){
  readBooksPromise(time, books[i])

  .then(function(sisawaktu){
    if (i !== books.length-1 && sisawaktu!=0){
      execute(sisawaktu, books, i+1)
    }})
    
  .catch(function (habis) {  
    console.log(habis)
  })
}


execute(time, books, i=0)

 
//Lakukan hal yang sama dengan soal no.1, habiskan waktu selama 10000 ms (10 detik) untuk membaca semua buku dalam array books.!