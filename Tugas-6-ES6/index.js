// Soal 1
const Luas_PersegiPanjang = (s)=>{
  const luas = s*s
  return luas
}

const Keliling_PersegiPanjang = (s)=>{
  const keliling = 4*s
  return keliling
}

let sisi = 20

console.log(Luas_PersegiPanjang(sisi))
console.log(Keliling_PersegiPanjang(sisi))

// Soal 2
const newFunction= (firstName ,lastName) => {
  return{
    firstName,
    lastName,
      fullName() {
          console.log(firstName + " " + lastName)
      }
  }
}
//Driver Code 
newFunction("William", "Imoh").fullName() 


// Soal 3
// Diberikan sebuah objek sebagai berikut:
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

//Soal 4
// Kombinasikan dua array berikut menggunakan array spreading ES6
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

// soal 5
// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = "earth" 
const view = "glass" 

var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

