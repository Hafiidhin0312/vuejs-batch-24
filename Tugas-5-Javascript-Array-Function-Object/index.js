// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var urut = daftarHewan.sort()
for (var index = 0; index < urut.length; index++) {
    console.log(urut[index])
} 


// soal 2
function introduce(params) { 
 return "Nama saya " + params.name+", umur saya " + params.age +"  tahun, alamat saya di " + params.address+ ", dan saya punya hobby yaitu "+ params.hobby+" !"
 }

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) 


// soal 3
function hitung_huruf_vokal(data) {  
var datum = data.toLowerCase()
var total_huruf_vokal = 0

for (var i = 0; i < datum.length; i++) {

  if (datum[i]==='a'||datum[i]==='i'||datum[i]==='u'||datum[i]==='e'||datum[i]==='o') {
    total_huruf_vokal +=1
  }
}

return total_huruf_vokal
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2


// soal 4

function hitung(angka) { 
  return (angka - 2)+angka
 }

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8



